// quiz3.rs
// This quiz tests:
// - Generics
// - Traits
// An imaginary magical school has a new report card generation system written in Rust!
// Currently the system only supports creating report cards where the student's grade
// is represented numerically (e.g. 1.0 -> 5.5).
// However, the school also issues alphabetical grades (A+ -> F-) and needs
// to be able to print both types of report card!

// Make the necessary code changes in the struct ReportCard and the impl block
// to support alphabetical report cards. Change the Grade in the second test to "A+"
// to show that your changes allow alphabetical grades.

// Execute `rustlings hint quiz3` or use the `hint` watch subcommand for a hint.

use std::fmt::{Display, Formatter, Error};

pub struct ReportCard {
    pub grade: f32,
    pub student_name: String,
    pub student_age: u8,
}

impl ReportCard {
    pub fn print(&self) -> String {
        format!("{} ({}) - achieved a grade of {}",
            &self.student_name, &self.student_age, &self.grade)
    }
    pub fn alpha_grade(&self) -> String {
        let mut agrade: String =
            ((self.grade.trunc() as u8 - 1 + ('A' as u8)) as char)
            .to_string();
        agrade.push_str(
            match self.grade.fract() {
                i if i < 1.0 / 3.0 => "+",
                i if i > 2.0 / 3.0 => "-",
                _ => "",
            });
        agrade
    }
}

impl Display for ReportCard {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "{} ({}) - achieved a grade of {}",
               &self.student_name, &self.student_age, &self.alpha_grade()
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generate_numeric_report_card() {
        let report_card = ReportCard {
            grade: 2.1,
            student_name: "Tom Wriggle".to_string(),
            student_age: 12,
        };
        assert_eq!(
            report_card.to_string(),
            "Tom Wriggle (12) - achieved a grade of B+"
        );
    }

    #[test]
    fn generate_alphabetic_report_card() {
        // TODO: Make sure to change the grade here after you finish the exercise.
        let report_card = ReportCard {
            grade: 1.0,
            student_name: "Gary Plotter".to_string(),
            student_age: 11,
        };
        assert_eq!(
            report_card.to_string(),
            "Gary Plotter (11) - achieved a grade of A+"
        );
    }
}
